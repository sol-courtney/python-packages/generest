"""CLI unit test module."""

import unittest

import click.testing

import generest.cli as cli


class TestCli(unittest.TestCase):

    """CLI unit test cases."""

    def test_cli_import(self):
        """Make sure we can import."""
        self.assertIsNotNone(cli)

    def test_cli_help(self):
        """Do a test on the --help command."""
        runner = click.testing.CliRunner()
        result = runner.invoke(cli.main, ['--help'])
        msg = f'code: {result.exit_code!s}, output: {result.output!r}'
        self.assertTrue(result.exit_code == 0, msg)

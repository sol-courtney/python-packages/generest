"""General unit test module."""

import unittest

import generest


class TestGeneral(unittest.TestCase):

    """General unit test cases."""

    def test_import(self):
        """Make sure we can import."""
        self.assertIsNotNone(generest)

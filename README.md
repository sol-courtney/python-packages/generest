[![PyPi Latest](https://img.shields.io/pypi/v/generest.svg)](https://pypi.org/project/generest/)
[![CircleCI](https://dl.circleci.com/status-badge/img/circleci/AtZu7a1zFfSHi3o4tjrgvt/Tp7h24z2BnpkTr4MkTYEvh/tree/main.svg?style=svg&circle-token=7523b0cd8ab68680c5642442518ae1bae9368272)](https://dl.circleci.com/status-badge/redirect/circleci/AtZu7a1zFfSHi3o4tjrgvt/Tp7h24z2BnpkTr4MkTYEvh/tree/main)

[![Build](https://gitlab.com/sol-courtney/python-packages/generest/badges/main/pipeline.svg)](https://gitlab.com/sol-courtney/python-packages/generest)
[![Codecov](https://codecov.io/gl/sol-courtney:python-packages/generest/branch/develop/graph/badge.svg)](https://codecov.io/gl/sol-courtney:python-packages/generest)
[![Docs](https://readthedocs.org/projects/generest/badge/?version=latest)](https://generest.readthedocs.io)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=sol-courtney_generest&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=sol-courtney_generest)

[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=sol-courtney_generest&metric=security_rating)](https://sonarcloud.io/summary/new_code?id=sol-courtney_generest)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=sol-courtney_generest&metric=vulnerabilities)](https://sonarcloud.io/summary/new_code?id=sol-courtney_generest)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=sol-courtney_generest&metric=bugs)](https://sonarcloud.io/summary/new_code?id=sol-courtney_generest)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=sol-courtney_generest&metric=reliability_rating)](https://sonarcloud.io/summary/new_code?id=sol-courtney_generest)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=sol-courtney_generest&metric=ncloc)](https://sonarcloud.io/summary/new_code?id=sol-courtney_generest)

[![Package Status](https://img.shields.io/pypi/status/generest.svg)](https://pypi.org/project/generest/)
[![PyVersions](https://img.shields.io/pypi/pyversions/generest.svg)](https://pypi.org/project/generest/)
[![PyPI Downloads](https://img.shields.io/pypi/dm/generest.svg?label=PyPI%20downloads)](https://pypi.org/project/generest/)

[![License](https://img.shields.io/pypi/l/generest.svg)](https://gitlab.com/sol-courtney/python-packages/generest/-/blob/main/LICENSE)

# Welcome to Generest (generate REST)
Python code generator for REST APIs.

---

[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-orange.svg)](https://sonarcloud.io/summary/new_code?id=sol-courtney_generest)

[![Quality gate](https://sonarcloud.io/api/project_badges/quality_gate?project=sol-courtney_generest)](https://sonarcloud.io/summary/new_code?id=sol-courtney_generest)
